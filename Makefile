include ./.env

.PHONY: env clone pull docker_build docker_push static init migrations server

env:
	pip install --upgrade pip
	pip install -r './requirements.txt'
	pip install -r './requirements-git.txt'


clone:	
	git clone $(GIT_PATH) .

pull:
	git pull $(GIT_PATH)

docker_build:
	docker-machine start default
	eval "$(docker-machine env default)"
	docker build -t $(DOCKER_IMAGE) .

docker_push:	
	docker login -u $(DOCKER_USER) -p $(DOCKER_PASSWORD)
	docker-machine start default
	eval "$(docker-machine env default)"
	docker push $(DOCKER_IMAGE)

static:
	python ./manage.py collectstatic --no-input

init:
	python ./manage.py migrate --no-input
	python ./manage.py collectstatic --no-input
	python ./manage.py createsuperuser

migrations:
	python ./manage.py makemigrations --no-input

migrate:
	python ./manage.py migrate --no-input

server:
	python ./manage.py runserver