import os  # isort:skip
from os.path import normpath, join
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse_lazy

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 10
}

INSTALLED_APPS = (
    'djangocms_admin_style',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'cms',
    'menus',
    'sekizai',
    'treebeard',
    'djangocms_text_ckeditor',
    'filer',
#    'djangocms_file',
    'djangocms_link',
    'easy_thumbnails',

    'Web_mvc',
    'configurations',

)

gettext = lambda s: s
DATA_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = False if str(os.environ.get('DEBUG', "false")) == "false" else True
FILER_DEBUG = DEBUG
THUMBNAIL_DEBUG = DEBUG

THUMBNAIL_HIGH_RESOLUTION = True
FILE_UPLOAD_PERMISSIONS = 0o644
FILE_UPLOAD_MAX_MEMORY_SIZE = 5242880 #50Mo
MAX_UPLOAD_SIZE = "5242880"

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!

ALLOWED_HOSTS = [host for host in os.environ.get('ALLOWED_HOSTS', "" if DEBUG == False else "*").replace(", ", ",").split(",")] #[]


# Application definition
ROOT_URLCONF = 'Web_mvc.urls'

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

TIME_ZONE = os.environ.get('TIME_ZONE', "Europe/Zurich")

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(DATA_DIR, 'static')

MEDIA_ROOT = os.path.join(DATA_DIR, 'media')

SITE_ID = 1


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            # os.path.join(BASE_DIR, 'theme', 'templates'),
            os.path.join(BASE_DIR, 'Web_mvc', 'templates'),
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.media',
                'django.template.context_processors.csrf',
                'django.template.context_processors.tz',
                'sekizai.context_processors.sekizai',
                'django.template.context_processors.static',
                'cms.context_processors.cms_settings',
            ],
            'libraries': {
                # 'sorl_thumbnail':'sorl.thumbnail.templatetags.thumbnail',
            },
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                #'django.template.loaders.eggs.Loader'
            ],
        },
    },
]


MIDDLEWARE = [
    'cms.middleware.utils.ApphookReloadMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware'
]

LANGS = os.environ.get("LANGUAGES", "fr-fr").replace(", ", ",").split(",")

USE_I18N = True if len(LANGS) > 1 else False

USE_L10N = USE_I18N

LANGUAGES = []
for lang in LANGS:
    LANGUAGES.append((lang, gettext(lang)))

LANGUAGE_CODE = LANGS[0]

if not os.environ.get('DEV', False):
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'handlers': {
            'file': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'filename': '/var/log/Web_mvc.log',
            },
        },
        'loggers': {
            'django': {
                'handlers': ['file'],
                'level': 'INFO',
                'propagate': True,
            },
        },
    }

GOOGLE_ANALYTICS_PROPERTY_ID = os.environ.get('GOOGLE_ANALYTICS_PROPERTY_ID', '')

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
     'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)
META_SITE_PROTOCOL = 'https'
META_USE_SITES = True

APPEND_SLASH = True

CMS_LANGUAGES = {
    'default': {
        'redirect_on_fallback': True,
        'public': True,
        'hide_untranslated': False,
    },
}

CMS_LANGUAGES[1] = []

for lg in LANGS:
    CMS_LANGUAGES[1].append({
            'code': lg,
            'hide_untranslated': False,
            'name': lg,
            'public': True,
        })

if len(LANGS) > 1:
    CMS_LANGUAGES[1][1]['redirect_on_fallback'] = True


DJANGOCMS_FORMS_PLUGIN_NAME = 'Formulaire'

# from theme import settings as ThemeSettings

# DJANGOCMS_FORMS_TEMPLATES = ThemeSettings.THEME_FORMS_TEMPLATES

# DJANGOCMS_VIDEO_ALLOWED_EXTENSIONS = ['mp4', 'webm', 'ogv']

# CRISPY_TEMPLATE_PACK = 'bootstrap4'

# DJANGOCMS_FORMS_WIDGET_CSS_CLASSES = {'__all__': ('form-control', ) }

# CMS_TEMPLATES = ThemeSettings.THEME_TEMPLATES

# CMS_PERMISSION = bool(os.environ.get('CMS_PERMISSION', False))

# CMS_PLACEHOLDER_CONF = {}

# CKEDITOR_SETTINGS = ThemeSettings.THEME_EDITOR_SETTINGS

# BLOG_IMAGE_FULL_SIZE = ThemeSettings.THEME_IMAGE_FULL_SIZE
# BLOG_IMAGE_THUMBNAIL_SIZE = ThemeSettings.THEME_IMAGE_THUMBNAIL_SIZE

# DJANGOCMS_ICON_SETS = ThemeSettings.THEME_ICON_SETS

# DJANGOCMS_BOOTSTRAP4_TAB_TEMPLATES = [
#     ('default', _('Default')),
# ]
# DJANGOCMS_BOOTSTRAP4_TAB_TEMPLATES += ThemeSettings.DJANGOCMS_BOOTSTRAP4_TAB_TEMPLATES


DATABASES = {}


if os.environ.get('POSTGRES_HOST', None) == None:
    default_db = {
        'ENGINE': "django.db.backends.sqlite3",
        'NAME': os.path.join(DATA_DIR, 'db.sqlite3'),
    }
elif os.environ.get('POSTGRES_HOST', None):
    default_db = {
            'ENGINE': "django.db.backends.postgresql_psycopg2",
            'HOST': os.environ.get('POSTGRES_HOST'),
            'NAME': os.environ.get('POSTGRES_NAME'),
            'USER': os.environ.get('POSTGRES_USER', os.environ.get('POSTGRES_NAME')),
            'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
            'PORT': "5432",
            }
    
DATABASES['default'] = default_db

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters'
)

# SEND_GRID_API_KEY = ''
# EMAIL_HOST = os.environ.get('EMAIL_HOST')
# EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
# EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')
# EMAIL_PORT = 587
# EMAIL_USE_TLS = True
# DEFAULT_FROM_EMAIL = os.environ.get('DEFAULT_FROM_EMAIL')
# ACCOUNT_EMAIL_SUBJECT_PREFIX = ''
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

# AUTH_PASSWORD_VALIDATORS = [
#     {
#         'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
#     },
# ]

# AUTHENTICATION_BACKENDS = (
#     'django.contrib.auth.backends.ModelBackend',
#     'allauth.account.auth_backends.AuthenticationBackend',
# )

# ACCOUNT_AUTHENTICATION_METHOD = "email"
# #ACCOUNT_USER_MODEL_USERNAME_FIELD = "first_name"
# #ACCOUNT_USER_DISPLAY='auth.User.first_name'
# ACCOUNT_FORMS = {
#     'signup':'accounts.forms.CustomSignupForm'
# }
# ACCOUNT_USERNAME_REQUIRED=False
# ACCOUNT_EMAIL_REQUIRED=True
# ACCOUNT_EMAIL_VERIFICATION = os.environ.get('ACCOUNT_EMAIL_VERIFICATION', "mandatory")
# ACCOUNT_DEFAULT_HTTP_PROTOCOL="https"
# ACCOUNT_SIGNUP_PASSWORD_ENTER_TWICE=True
# ACCOUNT_LOGIN_ON_EMAIL_CONFIRMATION=True
# ACCOUNT_CONFIRM_EMAIL_ON_GET=True
# ACCOUNT_LOGIN_ON_EMAIL_CONFIRMATION=True
# LOGIN_REDIRECT_URL = '/technologies/'
# LOGIN_URL = "/account/login"

# LOCALE_PATHS = (
#     os.path.join(BASE_DIR, 'theme/locale'),
# )

# if os.environ.get('BROKER_URL', None):
#     BROKER_URL = os.environ.get('BROKER_URL')
#     CELERY_ACCEPT_CONTENT = ['json']
#     CELERY_TASK_SERIALIZER = 'json'
#     CELERY_RESULT_SERIALIZER = 'json'
#     CELERY_TIMEZONE = os.environ.get('time_zone', "Europe/Zurich")
#     CELERY_ENABLE_UTC = True
#     CELERY_MESSAGE_COMPRESSION = 'gzip'
#     CELERY_TRACK_STARTED = True
# else:
#     pass

# BLOG_IMAGE_THUMBNAIL_SIZE = {'size': '340x200', 'crop': True, 'upscale': False}
# BLOG_IMAGE_FULL_SIZE = {'size': '2720 × 1600', 'crop': True,'upscale': False}
# BLOG_LATEST_POSTS = 3

FILER_FILE_MODELS = (
      'filer.Image',
      'filer.File',
)

# #Accounting
# if os.environ.get('POSTGRES_HOST', False):
#     DEFAULT_CURRENCY = "CHF"
#     HORDAK_DECIMAL_PLACES = 2
#     HORDAK_MAX_DIGITS = 13


# if os.environ.get('SAFERPAY_USERNAME', None):
#     USE_HTTPS = True
#     SAFERPAY_APIURL = 'https://test.saferpay.com/api' if os.environ.get('SAFERPAY_TEST') == True else 'https://test.saferpay.com/api'
#     SAFERPAY_USERNAME = os.environ.get('SAFERPAY_USERNAME')
#     SAFERPAY_PASSWORD = os.environ.get('SAFERPAY_PASSWORD')
#     SAFERPAY_CUSTOMERID = os.environ.get('SAFERPAY_CUSTOMERID') # a number
#     SAFERPAY_TERMINALID = os.environ.get('SAFERPAY_TERMINALID')  # a number
#     SAFERPAY_SUCCESS_CAPTURE_URL = reverse_lazy('payments:capture')  # your callback after getting the money
#     SAFERPAY_SUCCESS_URL = reverse_lazy('payments:done')  # your callback after a successfull order
#     SAFERPAY_FAIL_URL = reverse_lazy('payments:canceled')  # your callback after a failed payment
#     SAFERPAY_ORDER_TEXT_NR = _('Payment for ' + os.environ.get('SITE_NAME', SAFERPAY_USERNAME) + ', Order nr. %s.')
#     SAFERPAY_FORCE_LIABILITYSHIFT_ACTIVE = False  # default: False
#     SAFERPAY_DO_NOTIFY = True if os.environ.get('SAFERPAY_TEST') == True else False # default: False

# from decimal import Decimal
# INVOICING_TAX_RATE = Decimal(7.7)

# import json
# INVOICING_SUPPLIER = {
#     'name': 'Waser Technologies',
#     'street': 'Rue du Vallon 26',
#     'city': 'Lausanne',
#     'zip': '1005',
#     'country_code': 'CH',
#     'registration_id': '',
#     'tax_id': '',
#     'vat_id': '',
#     'additional_info': json.dumps({
#         "www": "www.waser.tech"
#         #... legal matters
#     }),
#     'bank': {
#         'name': 'UBS Switzerland AG',
#         'street': 'Place Saint-François 16',
#         'zip': '1003',
#         'city': 'Lausanne',
#         'country_code': 'CH',
#         'iban': 'CH560024324354151402W',
#         'swift_bic': 'UBSWCHZH80A'
#     }
# }

# INVOICING_SUPPLIER_LOGO_URL = normpath(join(STATIC_URL, 'logo_blue.png'))

# #from invoicing.models import Invoice
# INVOICING_COUNTER_PERIOD = 'monthly' #Invoice.COUNTER_PERIOD.MONTHLY

# # Remember to set INVOICING_NUMBER_FORMAT manually to match preferred way of invoice numbering schema.
# # For example if you choose reset counter on daily basis, you need to use in INVOICING_NUMBER_FORMAT
# # at least {{ invoice.date_issue|date:'d/m/Y' }} to distinguish invoice's full numbers between days.
# INVOICING_NUMBER_FORMAT = "{{ invoice.date_issue|date:'Y/m' }}/{{ invoice.pk }}"