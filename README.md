# Web.mvc Manager

As it's name emplies, Web.mvc Manager creates instances of Web.mvc and manages them with Jelastic API.

## Installation

### Clone the project
`git clone git@gitlab.com:waser-technologies/web.mvc-manager.git .`

### Create environment variables file
`cp .env.exemple .env`

Edit the variables inside *.env* file.

### Installation

You must allow the variables contained inside .env to be passed with [direnv](https://direnv.net).
Create an environment and initiate Web.mvc.

```
direnv allow .
make env
make init
```

Web.mvc initialisation will prompt you to create an administrator account for the developpment website.

## Usage

### Run developpment server
`make server`

### Deploy
To deploy Web.mvc Manager, checkout the [Deploy Web.mvc](https://gitlab.com/waser-technologies/technologies/web.mvc#hosting) section [@Web.mvc](https://gitlab.com/waser-technologies/technologies/web.mvc).
