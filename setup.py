import warnings

from distutils.core import setup

package = __import__('configurations')

try:
    README = open('README.md').read()
except:
    warnings.warn('Could not read README.md')
    README = None

try:
    REQUIREMENTS = open('requirements.txt').read()
except:
    warnings.warn('Could not read requirements.txt')
    REQUIREMENTS = None


setup(
    name='web-mvc-configuration',
    version="1.4.6",
    description=(
        'VAT app for Web.mvc.'
    ),
    long_description=README,
    install_requires=REQUIREMENTS,
    author='Danny Waser',
    author_email='danny@waser.tech',
    url='https://gitlab.con/waser-technologies/web.mvc-apps/configurations',
    packages=[
        'configurations',
        'configurations.migrations',
    ],
    include_package_data=True,
    classifiers=[
        'Development Status :: 6 - Mature',
        'Environment :: Web Environment',
        'Framework :: Django, Django-SHOP',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Utilities'
    ],
)
