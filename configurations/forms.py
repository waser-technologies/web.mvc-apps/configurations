from django import forms
from djangocms_link.fields import PageSearchField

from .models import Legal

class LegalForm(forms.ModelForm):
    terms = PageSearchField()
    privacy = PageSearchField()

    class Meta:
        model = Legal
        fields = "__all__"