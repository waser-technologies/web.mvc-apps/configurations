from django.contrib import admin

# Register your models here.
from .models import Legal
from .forms import LegalForm

class LegalAdmin(admin.ModelAdmin):
    form = LegalForm

admin.site.register(Legal, LegalAdmin)