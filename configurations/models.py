from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import Page, Site


class Legal(models.Model):
    site = models.OneToOneField(Site, on_delete=models.CASCADE, verbose_name=_("Site"))
    terms = models.ForeignKey(Page, on_delete=models.CASCADE, related_name="terms_and_conditions", verbose_name=_("Terms and Conditions"))
    privacy = models.ForeignKey(Page, on_delete=models.CASCADE, related_name="privacy_policy", verbose_name=_("Privacy policy"))

    def __str__(self):
        return self.site.name